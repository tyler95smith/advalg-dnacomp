

#dna1 = "GCATGCA"
#dna2 = "GATTACA"

dna1 = "atgtt"
dna2 = "atcgtac"

PHuman = ""
Neandertal = ""

fileReader = open("DNA/ProtoHuman.txt", "r")

if fileReader.mode == "r":
	PHuman = fileReader.read()

fileReader.close()

fileReader = open("DNA/Neandertal.txt", "r")

if fileReader.mode == "r":
	Neandertal = fileReader.read()

fileReader.close()

print("PHuman: {0}").format(len(PHuman))
print("Neandertal: {0}").format(len(Neandertal))

scoreGrid = dict()
matchGrid = []
matchDict = dict()


def print2DGrid(grid):
	for i in range(0,len(grid)):
		print(grid[i])


def initScoreGrid(grid):
	grid["a","a"] = 5

	grid["a","c"] = -1 
	grid["c","a"] = -1

	grid["a","g"] = -2 
	grid["g","a"] = -2

	grid["a","t"] = -1 
	grid["t","a"] = -1

	grid["a","-"] = -3 
	grid["-","a"] = -3

	grid["c","c"] = 5

	grid["c","g"] = -3 
	grid["g","c"] = -3

	grid["c","t"] = -2 
	grid["t","c"] = -2

	grid["c","-"] = -4 
	grid["-","c"] = -4

	grid["g","g"] = 5

	grid["g","t"] = -2 
	grid["t","g"] = -2

	grid["g","-"] = -2 
	grid["-","g"] = -2

	grid["t","t"] = 5 

	grid["t","-"] = -1 
	grid["-","t"] = -1

	grid["-","-"] = -100


def fillMatchGrid(str1,str2):
	#init array
	print("start init")
	for i in range(0,len(str1)+1):
		matchGrid.append([])
		for j in range(0,len(str2)+1):
				matchGrid[i].append(0)

	print("start basecase")
	#fill array base cases
	matchGrid[0][0] = 0
	for i in range(1,len(str1)+1):
		matchGrid[i][0] = scoreGrid[(str1[i-1], "-")]*i
	for j in range(1,len(str2)+1):
		matchGrid[0][j] = scoreGrid[str2[j-1],"-"]*j


	print("start fill proper")
	#fill array proper
	for i in range(1,len(str1)+1):
		for j in range(1,len(str2)+1):
			match = matchGrid[i-1][j-1] + scoreGrid[str1[i-1],str2[j-1]]
			delete = matchGrid[i-1][j] + scoreGrid[str1[i-1],'-']
			insert = matchGrid[i][j-1] + scoreGrid[str2[j-1],'-']
			matchGrid[i][j] = max(match,delete,insert)
			if (i%100 == 0 and j%16000 == 0):
				print("i: {0}").format(i)


def fillMatchGridv2(str1,str2):
	#init array
	print("start init")
	for i in range(0,len(str1)+1):
		matchGrid.append([])
		#for j in range(0,len(str2)+1):
		#		matchGrid[i].append(0)

	print("start basecase")
	#fill array base cases
	matchGrid[0].append(0)
	for i in range(1,len(str1)+1):
		matchGrid[i].append(scoreGrid[(str1[i-1], "-")]*i)
	for j in range(1,len(str2)+1):
		matchGrid[0].append(scoreGrid[str2[j-1],"-"]*j)


	print("start fill proper")
	#fill array proper
	for i in range(1,len(str1)+1):
		for j in range(1,len(str2)+1):
			match = matchGrid[i-1][j-1] + scoreGrid[str1[i-1],str2[j-1]]
			delete = matchGrid[i-1][j] + scoreGrid[str1[i-1],'-']
			insert = matchGrid[i][j-1] + scoreGrid[str2[j-1],'-']
			matchGrid[i].append(max(match,delete,insert))
			if (i%100 == 0 and j%16000 == 0):
				print("i: {0}").format(i)


def fillMatchDict(str1,str2):
	#init array
	#print("start init")
	#for i in range(0,len(str1)+1):
	matchDict[0,0] = 0
		#for j in range(0,len(str2)+1):
		#		matchGrid[i].append(0)

	#print("start basecase")
	#fill array base cases
	#matchGrid[0].append(0)
	for i in range(1,len(str1)+1):
		matchDict[i,0] = scoreGrid[(str1[i-1], "-")]*i
	for j in range(1,len(str2)+1):
		matchDict[0,j] = scoreGrid[str2[j-1],"-"]*j


	print("start fill proper")
	#fill array proper
	for i in range(1,len(str1)+1):
		for j in range(1,len(str2)+1):
			match = matchDict[i-1,j-1] + scoreGrid[str1[i-1],str2[j-1]]
			delete = matchDict[i-1,j] + scoreGrid[str1[i-1],'-']
			insert = matchDict[i,j-1] + scoreGrid[str2[j-1],'-']
			matchDict[i,j] = max(match,delete,insert)
			if (i%100 == 0 and j%16000 == 0):
				print("i: {0}").format(i)


def findBestAlignment(str1,str2):
	align1 = ""
	align2 = ""
	i = len(str1)
	j = len(str2)
	gapcount = 0
	file = open("Comparisons/PH-NE.txt","w")
	while (i > 0 or j > 0):
		#print("i: {0}").format(i)
		#print("j: {0}").format(j)
		#print("matchGrid[i][j] = {0}").format(matchGrid[i][j])
		#print("matchGrid[i-1][j-1] = {0}").format(matchGrid[i-1][j-1])
		#print("scoreGrid[str1[i-1],str2[j-1]] = {0}").format(scoreGrid[str1[i-1],str2[j-1]])
		if (i > 0 and j > 0 and matchGrid[i][j] == matchGrid[i-1][j-1] + scoreGrid[str1[i-1],str2[j-1]]):
			#align1 = str1[i-1] + align1
			#align2 = str2[j-1] + align2
			file.write(str1[i-1] + " " + str2[j-1] + "\n")
			i -= 1
			j -= 1
		elif (i > 0 and matchGrid[i][j] == matchGrid[i-1][j] + scoreGrid[str1[i-1],'-']):
			#align1 = str1[i-1] + align1
			#align2 = "-" + align2
			file.write(str1[i-1] + " -" + "\n")
			i -= 1
			gapcount += 1
		elif (j > 0 and matchGrid[i][j] == matchGrid[i][j-1] + scoreGrid[str2[j-1], '-']):
			#align1 = "-" + align1
			#align2 = str2[j-1] + align2
			file.write("- " + str2[j-1] + "\n")
			j -= 1
			gapcount += 1

	file.close()

	print("gapcount: {0}").format(gapcount)

	#print(align1)
	#print(align2)

	#print alignments vertically
	#for i in range(0,len(align1)):
	#	print(align1[i] + " " + align2[i])

def findBestAlignmentDict(str1,str2):
	align1 = ""
	align2 = ""
	i = len(str1)
	j = len(str2)
	gapcount = 0
	file = open("Comparisons/PH-NE.txt","w")
	while (i > 0 or j > 0):
		#print("i: {0}").format(i)
		#print("j: {0}").format(j)
		#print("matchGrid[i][j] = {0}").format(matchGrid[i][j])
		#print("matchGrid[i-1][j-1] = {0}").format(matchGrid[i-1][j-1])
		#print("scoreGrid[str1[i-1],str2[j-1]] = {0}").format(scoreGrid[str1[i-1],str2[j-1]])
		if (i > 0 and j > 0 and matchDict[i,j] == matchDict[i-1,j-1] + scoreGrid[str1[i-1],str2[j-1]]):
			#align1 = str1[i-1] + align1
			#align2 = str2[j-1] + align2
			file.write(str1[i-1] + " " + str2[j-1] + "\n")
			i -= 1
			j -= 1
		elif (i > 0 and matchDict[i,j] == matchDict[i-1,j] + scoreGrid[str1[i-1],'-']):
			#align1 = str1[i-1] + align1
			#align2 = "-" + align2
			file.write(str1[i-1] + " -" + "\n")
			i -= 1
			gapcount += 1
		elif (j > 0 and matchDict[i,j] == matchDict[i,j-1] + scoreGrid[str2[j-1], '-']):
			#align1 = "-" + align1
			#align2 = str2[j-1] + align2
			file.write("- " + str2[j-1] + "\n")
			j -= 1
			gapcount += 1

	file.close()

	print("gapcount: {0}").format(gapcount)

#Main

initScoreGrid(scoreGrid)
#print("scores initialized")

#fillMatchGridv2(dna1,dna2)
#print(matchGrid)
#print("grid filled")

fillMatchGridv2(PHuman,Neandertal)
#print("grid filled")

findBestAlignment(PHuman,Neandertal)

#fillMatchDict(PHuman,Neandertal)

#findBestAlignmentDict(PHuman,Neandertal)





