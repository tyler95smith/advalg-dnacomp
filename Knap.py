from random import randint
import time
import matplotlib.pyplot as plt
import matplotlib.lines as mlines

cache = dict()
cacheValid = dict()


def knapRecursive(objects,knap1,knap2):

	if knap1 < 0 or knap2 < 0:  # object placed in knap caused overflow
		return False
	if knap1 == 0 and knap2 == 0:  # both knaps full
		return True
	if not objects: # objects list is empty
		return False

	curObj = objects.pop()

	return knapRecursive(objects[:],knap1,knap2) or knapRecursive(objects[:],knap1-curObj,knap2) or knapRecursive(objects[:],knap1,knap2-curObj)


def knapMemo(objects,knap1,knap2):

	if knap1 < 0 or knap2 < 0:  # object placed in knap caused overflow
		return False
	if knap1 == 0 and knap2 == 0:  # both knaps full
		return True
	if not objects:  # objects list is empty
		return False

	curObj = objects.pop()

	if (curObj,knap1,knap2) in cache:
		return cache[(curObj,knap1,knap2)]
	else:
		tuplekey = (curObj,knap1,knap2)
		cache[tuplekey] = knapMemo(objects[:], knap1, knap2) or knapMemo(objects[:], knap1 - curObj,	knap2) or knapMemo(objects[:], knap1, knap2 - curObj)
		return cache[tuplekey]


def probListGenerator(count,max):
	itemList = []
	for item in range(0,count):
		size = randint(1,max-1)
		itemList.append(size)
	return itemList

maxCount = 800
maxSize = 500
knap1Size = 100
knap2Size = 100

recursivex = []
recursivey = []

recurCount = 10
while recurCount <= maxCount:
	for repeat in range(0,9):
		start_time = time.time()
		knapRecursive(probListGenerator(recurCount,maxSize)[:], knap1Size, knap2Size)
		total_time = (time.time() - start_time)
		recursivex.append(recurCount)
		recursivey.append(total_time)
	recurCount+=10

memox = []
memoy = []

memoCount = 10
while memoCount <= maxCount:
	for repeat in range(0,9):
		start_time = time.time()
		knapMemo(probListGenerator(memoCount,maxSize)[:], knap1Size, knap2Size)
		cache = dict()
		total_time = (time.time() - start_time)
		memox.append(memoCount)
		memoy.append(total_time)
	memoCount+=10

red_line = mlines.Line2D([],[],color='red',label='Memo Data')
blue_line = mlines.Line2D([],[],color='blue',label='Recursive Data')


plt.legend(handles=[red_line,blue_line])
plt.plot(memox,memoy,'r',recursivex,recursivey,'b')
plt.yscale('log')
plt.xlabel('item count')
plt.ylabel('time(s)')
plt.title('memoData vs recursiveData')
plt.grid(True)
plt.savefig("memovsrecurGraph.png")
plt.show()
